frontend_0906
===============

フロントエンド勉強会 vol.2のハンズオンで使用するプロジェクトです。

環境準備
----------------

### nodeとnpmを準備する

```bash
node -v
npm -v
```

バージョンが表示されない場合はnodeをインストールします。
windowsではnodistを使うのが便利です。[参考](http://qiita.com/satoyan419/items/56e0b5f35912b9374305)

nodeがインストールできたら、npmを更新します。

```bash
npm update -g npm
```

### gulp, coffee-script, browserifyを準備する

```bash
gulp --version
coffee --version
browserify --version
```

コマンドがなければインストールします。

```bash
npm install -g gulp
npm install -g coffee-script
npm install -g browserify
```

プレゼンを見たい
-------------------

```bash
cd presentation
npm install -g grunt-cli bower
npm install
bower install
grunt serve
```
