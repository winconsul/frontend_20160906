'use strict';

var COFFEE_DIR = 'coffee';
var JS_DIR = '.';

var gulp = require('gulp');
var watch = require('gulp-watch');
var notify = require('gulp-notify');
var browserify = require('browserify');
var filter = require('gulp-filter');
var source = require('vinyl-source-stream');
var rename = require('gulp-rename');
var path = require('path');

gulp.task('build', function(){
    return browserify({
               entries: [ COFFEE_DIR + '/main.coffee' ],
               extensions: [ '.coffee' ],
               transform: [ 'coffeeify' ]
           }).bundle()
             .on('error', function(){
                 var args = Array.prototype.slice.call(arguments);
                 notify.onError({
                     title: "Compile Error",
                     message: "<%= error %>"
                 }).apply(this, args);
                 this.emit("end");
             })
             .pipe(source('main.js'))
             .pipe(rename('bundle.js'))
             .pipe(gulp.dest(JS_DIR));
});

gulp.task('watch', function(){
    watch(COFFEE_DIR + '/**/*', function(){
    	gulp.start('build');
    });
});

gulp.task('default', [ 'build' ]);
