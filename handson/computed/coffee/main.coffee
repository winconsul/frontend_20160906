Vue = require 'vue'

new Vue
    el: "#main"
    data: () ->
        firstName: 'John'
        lastName:  'Doe'
        birth:
            year:  1982
            month: 5
            date:  24
    computed:
        fullName: () ->
            @firstName + ' ' + @lastName
