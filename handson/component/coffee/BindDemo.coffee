Vue = require 'vue'

BindDemo = Vue.extend
    template: """
              <table>
                <tr>
                  <th></th>
                  <th>親の値</th>
                  <th>子の値</th>
                  <th></th>
                </tr>
                <tr>
                  <td><slot name="title"></slot></td>
                  <td><slot name="parent"></slot></td>
                  <td><input v-model="value" /></td>
                  <td>{{ description }}</td>
                </tr>
              </table>
              """
    props: ['title', 'value', 'description']
    data: () ->
        {}

module.exports = BindDemo
