# フロントエンド勉強会 vol.2

---

## まずはclone

```
git clone https://git.winc.co.jp/git/wakamatsu/frontend_0906.git
```

---

## 今日やること

1. 算出プロパティ(ハンズオン)
2. module.exportsとrequire
3. コンポーネント(ハンズオン)

---

## 算出プロパティ

- - -
### 例えば

```
<div class="fullname">
    {{ firstName + ' ' + lastName }}
</div>
```

式がテンプレートにあるとメンテナンスが難しい

--

### そこで算出プロパティ

```
<div class="fullname">
    {{ fullName }}
</div>
```

```
data:
  firstName: 'John'
  lastName: 'Doe'
computed:
  fullName: () ->
    firstName + ' ' + lastName
```

関数をプロパティのように扱う

--

### やってみよう

下準備
handson/computedで次のコマンドを実行する

```bash
npm install
gulp build
gulp watch
```

- - -

1. handson/computed/index.htmlを開いて算出プロパティの動作を確認しよう
2. 生年月日それぞれの入力欄を作ろう
3. フルネームの後ろに(年齢)の形で今日時点での年齢を表示しよう

<small>解答例はexample/computed</small>

---

## module.exportsとrequire

- - -

依存関係の管理を提供する

1. 機能を実装してexportする
2. 使いたい機能をrequireする

```
// utils/add.js

module.exports = function(a, b) {
    return a + b;
}
```

```
// app.js

var add = require('./utils/add');

console.log(add(1, 2)); // #=> 3
```

--

### 既に出てきた例

```
# app.coffee

Vue = require 'vue'

# 以降 vue.jsの操作
```

### バージョンを混在させたい
```
# app.coffee

$1 = require 'jQuery1'
$2 = require 'jQuery2'

# 以降jQuery ver1系とjQuery ver2系が混在した処理
```

--

### requireを解決するのは？

今回は```gulp build```の中でbrowserifyというフレームワークを使って解決しています  
が、話しだすと長くなるので割愛

---

## コンポーネント

- - -

要は ** カプセル化 **  
適切な単位でhtml(および対応するJavaScriptのコード)を切り分ける

--

script  
```
# コンポーエントを定義する
NewComponent = Vue.extend
    template: '<div>Hello, New Component</div>'

# コンポーネントをVueに登録する
Vue.component 'newComponent', NewComponent
```
  
html
```
<div id="example">
    <new-component></new-component>
</div>
```

レンダリング
```
<div id="example">
    <div>Hello, New Component</div>
</div>
```

--

一部のタグは、子要素に含められるタグが限られている  
たとえば```ul```や```ol```は```li```のみを子要素にできる

#### BAD
```
<ol>
    <my-list-item></my-list-item>
</ol>
```

#### GOOD
```
<ol>
    <li is="my-list-item"></li>
</ol>
```

--

### プロパティ

script  
```
# コンポーエントを定義する
NewComponent = Vue.extend
    props: ['name'] # プロパティの名前を定義する
    template: '<div>Hello, {{ name }}</div>'

# コンポーネントをVueに登録する
Vue.component 'newComponent', NewComponent
```
  
html
```
<div id="example">
    <new-component name="New Component"></new-component>
</div>
```

--

### 動的プロパティ

script  
```
NewComponent = Vue.extend
    props: ['name']
    template: '<div>Hello, {{ name }}</div>'

Vue.component 'newComponent', NewComponent
```
  
html
```
<div id="example">
    <input v-model="userName"><br>
    <new-component v-bind:name="userName"></new-component>
</div>
```

html(省略版)
```
<div id="example">
    <input v-model="userName"><br>
    <new-component :name="userName"></new-component>
</div>
```

--

### バインドタイミング

#### one-way バインディング
* デフォルト
* 親の値を変更するとコンポーネントに反映する

#### two-way バインディング
* ```:value.sync="xxx"```のように記述する
* 親、子どちらで値を変更しても相互に反映する

#### one-time バインディング
* ```:value.once="xxx"```の用に記述する
* 親、子どちらで値を変更しても相互に反映**しない**

--

### 親子間通信

| メソッド           | 効果                             |
| ------------------ | -------------------------------- |
| ```$on()```        | イベントを待ち受ける             |
| ```$emit()```      | 自分に対するイベントを発火させる |
| ```$dispatch()```  | 親に向けてイベントを送る         |
| ```$broadcast()``` | 子に向けてイベントを送る         |

```
methods:
    click: () ->
        # 第一引数はイベント名, それ以降はイベントの引数
        @$dispatch 'button-is-clicked', @value
```

```
events:
    'button-is-clicked': (value) ->
        # do something
```

--

### キャメルケースとケバブケース

JavaScript中ではキャメルケースを使う
```
# コンポーエントを定義する
NewComponent = Vue.extend
    props: ['myName'] # プロパティの名前を定義する
    template: '<div>Hello, {{ myName }}</div>'

# コンポーネントをVueに登録する
Vue.component 'new-component', NewComponent
```
  
htmlではケバブケースを使う
```
<div id="example">
    <new-component my-name="New Component"></new-component>
</div>
```

--

他にも大事な機能がいっぱいあります  
解説は https://jp.vuejs.org/guide/components.html

--

### やってみよう

下準備  
handson/componentで次のコマンドを実行する

```bash
npm install
gulp build
gulp watch
```

- - -

1. バインディングの種類と動作を確認してみよう
2. BindDemoのinputが変更されたらページ上部にメッセージを表示してみよう
    * コンポーネント名はMessage
    * 変更通知はmethodsを使う
    * フォーマットは"(title)が変更されました。"

<small>解答例はexample/component</small>

---

### まとめ

* 算出プロパティはテンプレートで使う処理をコンポーネントに定義できる
* コンポーネントは複雑な機能を分割できる
* requireは機能をファイルごとに分割できる
