Vue = require 'vue'

Vue.component 'bindDemo', require './BindDemo'
Vue.component 'message', require './Message'

new Vue
    el: "#main"
    data: () ->
        oneWay: 'one way binding'
        twoWay: 'two way binding'
        oneTime: 'one time binding'
        message: ''
    events:
        'child-changed': (message) ->
            # TODO 変更通知を受ける
            @message = message
