Vue = require 'vue'

BindDemo = Vue.extend
    template: """
              <table>
                <tr>
                  <th></th>
                  <th>親の値</th>
                  <th>子の値</th>
                  <th></th>
                </tr>
                <tr>
                  <td><slot name="title"></slot></td>
                  <td><slot name="parent"></slot></td>
                  <td><input v-model="value" @keyup="changed" /></td>
                  <td>{{ description }}</td>
                </tr>
              </table>
              """
    props: ['title', 'value', 'description']
    data: () ->
        {}
    methods:
        changed: () ->
            @$dispatch 'child-changed', "#{@title}が変更されました。"

module.exports = BindDemo
