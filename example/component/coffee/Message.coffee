Vue = require 'vue'

Message = Vue.extend
    template: """
              <div>
                {{ message }}
              </div>
              """
    props: ['message']

module.exports = Message
