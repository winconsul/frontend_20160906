Vue = require 'vue'

new Vue
    el: "#main"
    data: () ->
        firstName: 'John'
        lastName:  'Doe'
        birth:
            year:  1982
            month: 5
            date:  24
    computed:
        fullName: () ->
            @firstName + ' ' + @lastName
        age: () ->
            mmdd = (month, date) ->
                month * 100 + date

            today = new Date
            age = @birth.year - today.getFullYear();

            return age + 1 if mmdd(today.getMonth() + 1, today.getDate()) > mmdd(@birth.month, @birth.date)
            return age
